<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Prevision;
use App\Models\Localidad;
use App\Models\Clima;
use Illuminate\Contracts\View\View;

class PrevisionController extends Controller
{
    /**
     * Muestra el formulario para crear una nueva previsión.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        // Obtener todas las localidades
        $localidades = Localidad::all();


        $climas = Clima::all();
        $prevision = null;

        return view('previsiones.create', compact('localidades', 'climas', 'prevision'));
    }

    /**
     * Almacena una nueva previsión en la base de datos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // Valida los datos del formulario
        $request->validate([
            'localidad_id' => 'required',
            'fecha' => 'required|date',
            'minima' => 'required|numeric|between:-50,50',
            'maxima' => 'required|numeric|between:-50,50|gt:minima',
            'clima_id' => 'required',
        ]);

        // Crea la previsión
        $prevision = Prevision::create([
            'localidad_id' => $request->localidad_id,
            'fecha' => $request->fecha,
            'minima' => $request->minima,
            'maxima' => $request->maxima,
            'clima_id' => $request->clima_id,
        ]);

        // Almacena la nueva previsión en la sesión
        session()->flash('prevision', $prevision);

        // Redirige al usuario a la página de creación de previsión con un mensaje de éxito
        return redirect()->route('previsiones.create')
            ->with('success', 'Previsión creada exitosamente.');
    }



    public function getPrevisionesByLocalidad($localidadId)
    {
        $localidad = Localidad::findOrFail($localidadId);
        $previsiones = $localidad->previsiones;
        return view('previsiones.prevision_table', compact('previsiones'));
    }

    /**
     * Muestra el formulario de edición para la previsión especificada por su ID.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        // Obtener la previsión por su ID
        $prevision = Prevision::findOrFail($id);

        // Obtener todos los clima
        $climas = Clima::all();
        $localidades = Localidad::all();

        // Pasar los datos a la vista de creación
        return view('previsiones.edit', compact('prevision', 'climas', 'localidades'));
    }

    /**
     * Actualiza la previsión especificada en la base de datos.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            
            'fecha' => 'required|date',
            'minima' => 'required|numeric|between:-50,50',
            'maxima' => 'required|numeric|between:-50,50|gt:minima',
            'clima_id' => 'required',
        ]);

        $prevision = Prevision::findOrFail($id);
        
        $prevision->fecha = $request->input('fecha');
        $prevision->minima = $request->input('minima');
        $prevision->maxima = $request->input('maxima');
        $prevision->clima_id = $request->input('clima_id');
        $prevision->save();

        return redirect()->route('previsiones.create', $prevision->id)
            ->with('success', 'Previsión actualizada exitosamente.');
    }
    /**
     * Elimina la previsión especificada de la base de datos.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $prevision = Prevision::findOrFail($id);
        $prevision->delete();

        return response()->json(['success' => 'Previsión eliminada exitosamente']);
    } 

    public function gestion()
    {
        // Obtener todas las previsiones
        $previsiones = Prevision::all();

        // Pasar las previsiones a la vista previsiones.gestion
        return view('previsiones.gestion', compact('previsiones'));
    }

    public function show($id)
    {

        $prevision = Prevision::findOrFail($id);


        return view('previsiones.show', compact('prevision'));
    }
    public function index()
    {
        $previsiones = Prevision::all();
        return view('previsiones.index', compact('previsiones'));
    }

    public function showPrevisiones($localidad_id)
    {
        $previsiones = Prevision::where('localidad_id', $localidad_id)->get();

        // Si deseas devolver los datos en formato HTML
        return view('previsiones.table', compact('previsiones'));
    }

    public function showSingle($id)
    {
        $prevision = Prevision::findOrFail($id);
        return view('previsiones.show_single', compact('prevision'));
    }
}
