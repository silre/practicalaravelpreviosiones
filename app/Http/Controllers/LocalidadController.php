<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Localidad;
use App\Models\Prevision;
use Illuminate\Support\Facades\Session;

class LocalidadController extends Controller
{
    public function index()
    {
        $localidades = Localidad::all();
        $selectedLocalidad = Session::get('selectedLocalidad', null);
        return view('previsiones.index', compact('localidades', 'selectedLocalidad'));
    }

    public function mostrarPrevisiones(Request $request)
    {
        $request->validate([
            'localidad' => 'required|exists:localidades,id',
        ]);
    
        $localidadId = $request->input('localidad');
    
        Session::put('selectedLocalidad', $localidadId);
    
        $previsiones = Prevision::where('localidad_id', $localidadId)->get();
        $localidades = Localidad::all();
    
        $selectedLocalidad = $localidadId; // Definir la variable aquí
    
        return view('previsiones.index', compact('localidades', 'previsiones', 'selectedLocalidad'));
    }
}

