<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/previsiones/create';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Método para redirigir al usuario autenticado después del inicio de sesión.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return \Illuminate\Http\Response
     */
    protected function authenticated(Request $request, $user)
    {
        // Redirigir al usuario a la vista de gestión de previsiones
        return redirect()->route('previsiones.create');
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect()->route('previsiones.index');
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
    
        if (Auth::attempt($credentials, $request->filled('remember'))) {
            return redirect()->intended('/previsiones/create');
        } else {
            return redirect()->back()->withErrors(['email' => 'Correo electrónico o contraseña incorrecta'])->withInput($request->only('email', 'remember'));
        }
    }
    

    public function redirectTo()
    {
        return route('previsiones.create'); // Redirigir a la vista create después del inicio de sesión
    }
}
