<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clima extends Model
{
    use HasFactory;

    public function previsiones()
    {
        return $this->hasMany(Prevision::class);
    }
    public function create()
{
    // Obtener todos los climas
    $climas = Clima::all();
    
    return view('previsiones.create', compact('climas'));
}
}