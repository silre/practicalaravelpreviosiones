<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prevision extends Model
{
    use HasFactory;
    protected $table = 'previsiones';


    public function localidad()
    {
        return $this->belongsTo(Localidad::class);
    }

    public function clima()
    {
        return $this->belongsTo(Clima::class);
    }
    protected $fillable = [
        'localidad_id', 
        'fecha',
        'minima',
        'maxima',
        'clima_id',
    ];

  
}