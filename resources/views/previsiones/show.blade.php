@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Detalles de la Previsión') }}</div>

                <div class="card-body">
                    @if ($previsiones->count() > 0)
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Mínima</th>
                                <th>Máxima</th>
                                <th>Clima</th>
                            </tr>
                        </thead>
                        <tbody id="previsiones-cuerpo">
                            @foreach($previsiones as $prevision)
                            <tr>
                                <td>{{ $prevision->fecha }}</td>
                                <td>{{ $prevision->minima }}</td>
                                <td>{{ $prevision->maxima }}</td>
                                <td><img src="{{ asset('images/' . $prevision->clima->imagen) }}" alt="{{ $prevision->clima->clima }}" width="50"></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                    <p>No se encontraron previsiones.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection