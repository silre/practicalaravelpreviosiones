@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Añadir Previsión') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('previsiones.store') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="localidad" class="col-md-4 col-form-label text-md-end">{{ __('Localidad') }}</label>

                            <div class="col-md-6">
                                <select id="localidad" class="form-control @error('localidad_id') is-invalid @enderror" name="localidad_id" required>
                                    <option value="" disabled selected>Seleccionar...</option>
                                    @foreach($localidades as $localidad)
                                    <option value="{{ $localidad->id }}">{{ $localidad->nombre }}</option>
                                    @endforeach
                                </select>

                                @error('localidad_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="fecha" class="col-md-4 col-form-label text-md-end">{{ __('Fecha') }}</label>

                            <div class="col-md-6">
                                <input id="fecha" type="date" class="form-control @error('fecha') is-invalid @enderror" name="fecha" value="{{ old('fecha') }}" required autocomplete="fecha">

                                @error('fecha')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="minima" class="col-md-4 col-form-label text-md-end">{{ __('Mínima') }}</label>

                            <div class="col-md-6">
                                <input id="minima" type="number" class="form-control @error('minima') is-invalid @enderror" name="minima" value="{{ old('minima') }}" required autocomplete="minima">

                                @error('minima')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="maxima" class="col-md-4 col-form-label text-md-end">{{ __('Máxima') }}</label>

                            <div class="col-md-6">
                                <input id="maxima" type="number" class="form-control @error('maxima') is-invalid @enderror" name="maxima" value="{{ old('maxima') }}" required autocomplete="maxima">

                                @error('maxima')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="clima" class="col-md-4 col-form-label text-md-end">{{ __('Clima') }}</label>

                            <div class="col-md-6">
                                <select id="clima" class="form-control @error('clima_id') is-invalid @enderror" name="clima_id" required>
                                    <option value="" disabled selected>Seleccionar...</option>
                                    @foreach($climas as $clima)
                                    <option value="{{ $clima->id }}">{{ $clima->clima }}</option>
                                    @endforeach
                                </select>

                                @error('clima')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Añadir') }}
                                </button>
                            </div>
                        </div>


                        <!-- TABLA NUEVA PREVISIÓN:  -->
                        <!-- Mensaje de éxito -->
                        @if(session('success'))
                        <div class="alert alert-success mt-4">
                            {{ session('success') }}
                        </div>
                        @endif
                        @if(session('prevision'))
                        <!-- Detalles de la nueva previsión -->
                        <div class="card mt-4">
                            <div class="card-header">{{ __('Detalles de la nueva previsión') }}</div>

                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Localidad</th>
                                            <th>Fecha</th>
                                            <th>Mínima</th>
                                            <th>Máxima</th>
                                            <th>Clima</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ session('prevision')->localidad->nombre }}</td>
                                            <td>{{ session('prevision')->fecha }}</td>
                                            <td>{{ session('prevision')->minima }}</td>
                                            <td>{{ session('prevision')->maxima }}</td>
                                            <td><img src="{{ asset('images/' . session('prevision')->clima->imagen) }}" alt="{{ session('prevision')->clima->clima }}" width="50"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endif


                    </form>
                </div>
            </div>






            <!-- FORMULARIO PARA EDITAR Y ELIMINAR PREVISIONES -->





            <div class="card mt-5">
                <div class="card-header">{{ __('Modificar previsión') }}</div>
                <div class="card-body">

                    <div class="row mb-3">
                        <div class="row justify-content-center">

                            <label for="localidad-modificar" class="col-md-4 col-form-label text-md-end">Selecciona una localidad para ver sus previsiones</label>
                            <div class="col-md-6">
                                <select id="localidad-modificar" class="form-control" name="localidad_id" required>
                                    <option value="" selected>Seleccionar...</option>
                                    @foreach($localidades as $localidad)
                                    <option value="{{ $localidad->id }}">{{ $localidad->nombre }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="button" id="ver-previsiones" class="btn btn-primary">
                                {{ __('Ver') }}
                            </button>
                        </div>
                    </div>


                    <!-- Tabla para mostrar las previsiones -->
                    <div id="previsiones-table" class="mt-3" style="display: none;">
                        <table class="table">

                            <tbody id="previsiones-body">
                                <!-- Filas de la tabla con previsiones -->
                            </tbody>
                        </table>
                        
                    </div>

                 

                    <!-- Formulario para editar previsión -->
                    <div id="formulario" class="mt-3" style="display: none;">

                        @include('previsiones/edit', ['prevision' => $prevision])

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    $(document).ready(function() {
        $('#ver-previsiones').click(function() {
            var localidadId = $('#localidad-modificar').val();
            if (localidadId) {
                $.ajax({
                    url: '/localidades/' + localidadId + '/previsiones',
                    type: 'GET',
                    success: function(response) {
                        $('#previsiones-body').html(response);
                        $('#prevision-edit-form').hide();
                        $('#previsiones-table').show();
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                    }
                });
            } else {
                $('#previsiones-table').hide();
            }
        });

    });

    $(document).on('click', '.edit-prevision', function() {
        // Muestra el formulario de edición
        $("#formulario").show();

        // Obtener el ID de la previsión
        var previsionId = $(this).data('id');
        var actionUrl = "{{ route('previsiones.edit', ['id' => ':id']) }}".replace(':id', previsionId);

        // Cargar el contenido del formulario de edición
        $.ajax({
            url: actionUrl,
            type: 'GET',
            success: function(response) {
                // Verificar si la respuesta contiene el formulario
                if ($(response).find('#edit').length) {
                    // Insertar el contenido en el formulario
                    $('#formulario').html(response);
                } else {
                    // Ocultar el formulario si no se encontró el formulario de edición
                    $("#formulario").hide();
                    console.log("No se encontró el formulario de edición en la respuesta.");
                }
            },
            error: function(xhr) {
                console.log(xhr.responseText);
            }
        });
    });


    $(document).on('click', '.delete-prevision', function() {
        var previsionId = $(this).data('id');
        var token = $(this).data('token');
        var button = $(this);

        if (confirm("¿Estás seguro de que deseas eliminar esta previsión?")) {

            $.ajax({
                url: '/previsiones/' + previsionId,
                type: 'DELETE',
                data: {
                    '_token': token
                },
                success: function(response) {
                    alert(response.success);
                    // Recargar la página
                    location.reload();
                },
                error: function(xhr) {
                    console.log(xhr.responseText);
                }
            });
        }
    });

// FORMULARIO DE ACTUALIZAR

$(document).on('submit', '#edit', function(event) {
    event.preventDefault();

    var formData = $(this).serialize();
    var actionUrl = $(this).attr('action');

    $.ajax({
        url: actionUrl,
        type: 'POST', // Utiliza POST en lugar de PUT
        data: formData,
        success: function(response) {
            // Actualizar la fila de la tabla con la previsión actualizada
            var updatedPrevisionId = $('#prevision_id').val();
            $.ajax({
                url: '/previsiones/' + updatedPrevisionId,
                type: 'GET',
                success: function(response) {
                    $('#prevision_' + updatedPrevisionId).replaceWith(response);
                    
                    // Ocultar el formulario de edición
                    $("#formulario").hide();

                    // Mostrar mensaje de éxito
                    alert('Previsión actualizada exitosamente.');

                    // Recargar la página
                    location.reload();
                },
                error: function(xhr) {
                    console.log(xhr.responseText);
                }
            });
        },
        error: function(xhr) {
            console.log(xhr.responseText);
        }
    });
});


</script>

@endsection