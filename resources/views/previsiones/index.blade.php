@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Selecciona una localidad') }}</div>

                <div class="card-body">
                    <form id="previsiones-form" method="GET" action="{{ route('previsiones.mostrar') }}">

                        @csrf

                        <div class="row mb-3">
                            <label for="localidad" class="col-md-4 col-form-label text-md-end">{{ __('Localidad') }}</label>

                            <div class="col-md-6">
                                <select id="localidad" class="form-control @error('localidad') is-invalid @enderror" name="localidad" required>
                                    @foreach($localidades as $localidad)
                                    <option value="{{ $localidad->id }}" {{ (old('localidad') == $localidad->id || $selectedLocalidad == $localidad->id) ? 'selected' : '' }}>{{ $localidad->nombre }}</option>
                                    @endforeach
                                </select>

                                @error('localidad')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Ver tiempo') }}
                                </button>
                            </div>
                        </div>



                        <!-- Mostrar la tabla de previsiones -->
                        @if(isset($previsiones) && $previsiones->count() > 0)
                        <div class="mt-3">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Mínima</th>
                                        <th>Máxima</th>
                                        <th>Clima</th>
                                    </tr>
                                </thead>
                                <tbody id="previsiones-cuerpo">
                                    @foreach($previsiones as $prevision)
                                    <tr>
                                        <td>{{ $prevision->fecha }}</td>
                                        <td>{{ $prevision->minima }}</td>
                                        <td>{{ $prevision->maxima }}</td>
                                        <td><img src="{{ asset('images/' . $prevision->clima->imagen) }}" alt="{{ $prevision->clima->clima }}" width="50"></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif




                    </form>
                </div>
            </div>


        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    // Espera a que el documento esté cargado completamente
    document.addEventListener('DOMContentLoaded', function() {
        const localidadSelect = document.getElementById('localidad');

        // Obtén el valor de la localidad seleccionada almacenada en el LocalStorage
        const storedLocalidad = localStorage.getItem('selectedLocalidad');
        if (storedLocalidad) {
            localidadSelect.value = storedLocalidad;
        }

        // Agrega un event listener al cambio del select
        localidadSelect.addEventListener('change', function() {
            // Almacena la localidad seleccionada en el LocalStorage
            localStorage.setItem('selectedLocalidad', localidadSelect.value);
        });
    });
</script>
@endsection