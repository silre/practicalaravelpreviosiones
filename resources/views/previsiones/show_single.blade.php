@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Detalles de la previsión') }}</div>

                <div class="card-body">



                    <table class="table">
                        <thead>
                            <tr>
                                <th>Localidad</th>
                                <th>Fecha</th>
                                <th>Mínima</th>
                                <th>Máxima</th>
                                <th>Clima</th>
                            </tr>
                        </thead>


                        <tr>
                            <td>{{ $prevision->localidad->nombre }}</td>
                            <td>{{ $prevision->fecha }}</td>
                            <td>{{ $prevision->minima }}</td>
                            <td>{{ $prevision->maxima }}</td>
                            <td><img src="{{ asset('images/' . $prevision->clima->imagen) }}" alt="{{ $prevision->clima->clima }}" width="50"></td>
                        </tr>


                    </table>



                </div>
            </div>
        </div>
    </div>
</div>
@endsection