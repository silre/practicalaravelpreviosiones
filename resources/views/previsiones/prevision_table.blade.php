<table class="table">
    <thead>
        <tr>
            <th>Fecha</th>
            <th>Mínima</th>
            <th>Máxima</th>
            <th>Clima</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody id="previsiones-body">
        {{-- Aquí se mostrarán las previsiones --}}
        @foreach($previsiones as $prevision)
<tr>
    <td>{{ $prevision->fecha }}</td>
    <td>{{ $prevision->minima }}</td>
    <td>{{ $prevision->maxima }}</td>
    <td><img src="{{ asset('images/' . $prevision->clima->imagen) }}" alt="{{ $prevision->clima->clima }}" width="50"></td>
    <td>
    <button type="button" class="btn btn-primary edit-prevision" data-id="{{ $prevision->id }}" data-token="{{ csrf_token() }}" >Editar</button>
        <button type="button" class="btn btn-danger delete-prevision" data-id="{{ $prevision->id }}" data-token="{{ csrf_token() }}">Eliminar</button>
    </td>
</tr>
@endforeach
    </tbody>
</table>





