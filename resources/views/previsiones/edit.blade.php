@if(isset($prevision))
<div id="formulario">
    <form id="edit" method="POST" action="{{ route('previsiones.update', $prevision->id) }}">
        @csrf
        @method('PUT')
        <input type="hidden" id="prevision_id" name="prevision_id" value="{{ $prevision->id }}">

        <div class="row mb-3">
            <label for="fecha" class="col-md-4 col-form-label text-md-end">{{ __('Fecha') }}</label>
            <div class="col-md-6">
                <input id="fecha" type="date" class="form-control @error('fecha') is-invalid @enderror" name="fecha" value="{{ $prevision->fecha }}" required>
                @error('fecha')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="row mb-3">
            <label for="minima" class="col-md-4 col-form-label text-md-end">{{ __('Mínima') }}</label>
            <div class="col-md-6">
                <input id="minima" type="number" class="form-control @error('minima') is-invalid @enderror" name="minima" value="{{ $prevision->minima }}" required>
                @error('minima')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="row mb-3">
            <label for="maxima" class="col-md-4 col-form-label text-md-end">{{ __('Máxima') }}</label>
            <div class="col-md-6">
                <input id="maxima" type="number" class="form-control @error('maxima') is-invalid @enderror" name="maxima" value="{{ $prevision->maxima }}" required>
                @error('maxima')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="row mb-3">
            <label for="clima" class="col-md-4 col-form-label text-md-end">{{ __('Clima') }}</label>
            <div class="col-md-6">
                <select id="clima" class="form-control @error('clima_id') is-invalid @enderror" name="clima_id" required>
                    @foreach($climas as $clima)
                    <option value="{{ $clima->id }}" {{ $clima->id == $prevision->clima_id ? 'selected' : '' }}>{{ $clima->clima }}</option>
                    @endforeach
                </select>
                @error('clima_id')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>
        <div class="row mb-0">
            <div class="col-md-8 offset-md-4">
                <button type="submit" class="btn btn-primary">{{ __('Editar') }}</button>
            </div>
        </div>
</div>
</form>

</div>

@else
<div class="alert alert-danger" role="alert">
    No se encontró la previsión.
</div>
@endif