<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\PrevisionController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\LocalidadController;

Route::get('/', [LocalidadController::class, 'index'])->name('previsiones.index');

// Route::get('/show', [LocalidadController::class, 'mostrarPrevisiones'])->name('previsiones.show');
// Route::get('/previsiones/{localidad}', [PrevisionController::class, 'showPrevisiones'])->name('previsiones.showPrevisiones');
// Route::get('/localidades/{localidad}/previsiones', [LocalidadController::class, 'mostrarPrevisiones']);
Route::get('/previsiones/mostrar', [LocalidadController::class, 'mostrarPrevisiones'])->name('previsiones.mostrar');



Auth::routes();
// Rutas de autenticación
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login']);
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

//Al iniciar sesión va a la view create
Route::get('/previsiones/create', [PrevisionController::class, 'create'])->name('previsiones.create');

//Ruta para almacenar previsiones: 
Route::post('/previsiones', [PrevisionController::class, 'store'])->name('previsiones.store'); 

//Al crear una previsión va a show_single.blade: 
Route::get('/previsiones/{id}', [PrevisionController::class, 'showSingle'])->name('previsiones.show_single');


// Ruta para obtener las previsiones de una localidad
Route::get('/localidades/{localidad}/previsiones', [PrevisionController::class, 'getPrevisionesByLocalidad']);


// Rutas para editar y actualizar previsiones
Route::get('/previsiones/{id}/edit', [PrevisionController::class, 'edit'])->name('previsiones.edit');

Route::put('previsiones/{id}', [PrevisionController::class, 'update'])->name('previsiones.update');

// Ruta para eliminar previsiones
Route::delete('/previsiones/{id}', [PrevisionController::class, 'destroy'])->name('previsiones.destroy');
